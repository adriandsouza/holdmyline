package com.example.holdmyline;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.holdmyline.Model.User;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.rengwuxian.materialedittext.MaterialEditText;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

public class signup extends AppCompatActivity {
    MaterialEditText EditPhone,EditName,editPassword;
    Button btnSignUp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        EditName=(MaterialEditText)findViewById(R.id.EditName);
        EditPhone=(MaterialEditText)findViewById(R.id.EditPhone);
        editPassword=(MaterialEditText)findViewById(R.id.editPassword);
        btnSignUp=(Button)findViewById(R.id.btnSignUp);


    //    if(EditName.getequals("")|| EditPhone.equals("")||editPassword.equals("")){
       //    btnSignUp.setEnabled(false);
      //  } else {
      //      btnSignUp.setEnabled(true);
      //  }


        //Init Firebase
        final FirebaseDatabase database= FirebaseDatabase.getInstance();
        final DatabaseReference table_user=database.getReference("user");

        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final ProgressDialog mDialog=new ProgressDialog(signup.this);
                        mDialog.setMessage("Please Wait");
                mDialog.show();
    //           if(editPassword==null){
           //        mDialog.dismiss();
                     //     throw new IllegalArgumentException("Books cannot be null");
       //           Toast.makeText(signup.this,"Enter all details", Toast.LENGTH_SHORT).show();
        //       }

                table_user.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange( DataSnapshot dataSnapshot) {

                        if (dataSnapshot.child(EditName.getText().toString()).exists()||dataSnapshot.child(editPassword.getText().toString()).exists()){
                               mDialog.dismiss();
                                Toast.makeText(signup.this, "Enter All Details", Toast.LENGTH_LONG).show();
                           }
                         if (dataSnapshot.child(EditPhone.getText().toString()).exists())
                         {
                             mDialog.dismiss();
                             Toast.makeText(signup.this, "Phone Number already registered", Toast.LENGTH_SHORT).show();
                         }

                            else {
                                mDialog.dismiss();
                                User user = new User(EditName.getText().toString(), editPassword.getText().toString());
                                table_user.child(EditPhone.getText().toString()).setValue(user);
                                Toast.makeText(signup.this, "SignUp Successful", Toast.LENGTH_SHORT).show();
                                finish();


                            }



                        }
                    @Override
                    public void onCancelled(DatabaseError databaseError){

                    }



                    });
                }

            });
        }

    }

